#!/usr/bin/env bash

DEFAULT_KUBECONFIG="$HOME/.kube/config"
if [[ -f ${DEFAULT_KUBECONFIG} ]]; then
  export KUBECONFIG=${DEFAULT_KUBECONFIG}
fi

KUBECONFIG_DIR="$HOME/.kube/config.d"
mkdir -p ${KUBECONFIG_DIR}

ADDITIONAL_KUBECONFIGS=$(find ${KUBECONFIG_DIR} -type f -printf '%p:')
if [[ "${ADDITIONAL_KUBECONFIGS}" != "" ]]; then
  export KUBECONFIG="${ADDITIONAL_KUBECONFIGS}${KUBECONFIG}"
fi
echo KUBECONFIG=${KUBECONFIG}
